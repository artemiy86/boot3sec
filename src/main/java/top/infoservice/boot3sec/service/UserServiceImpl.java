package top.infoservice.boot3sec.service;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import top.infoservice.boot3sec.dto.RegistrationDto;
import top.infoservice.boot3sec.model.Role;
import top.infoservice.boot3sec.model.User;
import top.infoservice.boot3sec.repository.RoleRepository;
import top.infoservice.boot3sec.repository.UserRepository;
import top.infoservice.boot3sec.service.interfaces.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User findById(Long id) {
        User user = userRepository.findById(id).orElse(null);
        if(user != null) {

        }
        return user;
    }

    @Override
    public boolean save(User user) {
        userRepository.save(user);
        return true;
    }

    @Override
    public User userRegistration(RegistrationDto registrationDto) {
        Role defaultRole =  roleRepository.findById((long)1).get();
        List<Role> roleList = new ArrayList<>();
        roleList.add(defaultRole);
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String bCyptPassword = encoder.encode(registrationDto.getPassword());
        User newUser = User.builder()
                .email(registrationDto.getEmail())
                .name(registrationDto.getName())
                .password(bCyptPassword)
                .roles(roleList)
                .isActivate(true)
                .build();
        userRepository.save(newUser);
        return newUser;
    }

}
