package top.infoservice.boot3sec.service;

import jakarta.transaction.Transactional;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import top.infoservice.boot3sec.model.User;
import top.infoservice.boot3sec.security.jwt.JwtUser;
import top.infoservice.boot3sec.security.jwt.JwtUserFactory;
import top.infoservice.boot3sec.service.interfaces.UserService;


@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final UserService userService;

    public UserDetailsService(UserService userService) {
        this.userService = userService;
    }


    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.findByEmail(username).orElse(null);
        if(user == null) {
            throw new UsernameNotFoundException("Пользователь с e-mail адресом " + username + " не найден");
        }
        JwtUser jwtUser = JwtUserFactory.create(user);
        return jwtUser;
    }
}
