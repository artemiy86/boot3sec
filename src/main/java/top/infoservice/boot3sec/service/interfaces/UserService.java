package top.infoservice.boot3sec.service.interfaces;
import org.springframework.security.core.userdetails.UserDetailsService;
import top.infoservice.boot3sec.dto.RegistrationDto;
import top.infoservice.boot3sec.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> findByEmail(String email);
    User findById(Long id);
    boolean save(User user);

    User userRegistration(RegistrationDto registrationDto);
}
