package top.infoservice.boot3sec.config.jwt;

import io.jsonwebtoken.Claims;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import top.infoservice.boot3sec.model.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUtils {

    public static JwtAuthentication generate(Claims claims) {
        final JwtAuthentication jwtInfoToken = new JwtAuthentication();
        jwtInfoToken.setRoles(getRoles(claims));
        jwtInfoToken.setFirstName(claims.get("firstName", String.class));
        jwtInfoToken.setUsername(claims.getSubject());
        return jwtInfoToken;
    }

    private static List<GrantedAuthority> getRoles(Claims claims) {

        List<Map<String,String>> map = claims.get("roles",List.class);
        List<GrantedAuthority> roles = new ArrayList<>();
        for(Map<String, String> el : map) {
            Role role = new Role(el.get("authority"));
            roles.add(new SimpleGrantedAuthority(role.getName()));
        }
        return roles;
    }

}
