package top.infoservice.boot3sec.dto.jwt;

import lombok.Data;

@Data
public class JwtRequest {
    private String email;
    private String password;
}
