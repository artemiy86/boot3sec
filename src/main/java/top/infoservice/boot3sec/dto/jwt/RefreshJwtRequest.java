package top.infoservice.boot3sec.dto.jwt;

import lombok.Data;

@Data
public class RefreshJwtRequest {
    public String refreshToken;
}
