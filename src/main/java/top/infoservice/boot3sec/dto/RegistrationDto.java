package top.infoservice.boot3sec.dto;

import lombok.Data;

@Data
public class RegistrationDto {
    private String email;
    private String name;
    private String password;
}
