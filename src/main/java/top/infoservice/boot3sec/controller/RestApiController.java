package top.infoservice.boot3sec.controller;

import jakarta.security.auth.message.AuthException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import top.infoservice.boot3sec.dto.RegistrationDto;
import top.infoservice.boot3sec.dto.jwt.JwtRequest;
import top.infoservice.boot3sec.dto.jwt.JwtResponse;
import top.infoservice.boot3sec.model.User;
import top.infoservice.boot3sec.model.validators.UserValidator;
import top.infoservice.boot3sec.security.jwt.JwtUser;
import top.infoservice.boot3sec.service.AuthService;
import top.infoservice.boot3sec.service.interfaces.UserService;

import java.util.*;

@RestController
@RequestMapping("/api/v1")
public class RestApiController {

    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final AuthService authService;
    private final UserValidator userValidator;

    public RestApiController(AuthenticationManager authenticationManager, UserService userService, AuthService authService, UserValidator userValidator) {
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.authService = authService;
        this.userValidator = userValidator;
    }

    @PostMapping("/auth/register")
    public ResponseEntity<User> registration(@RequestBody @Valid RegistrationDto registrationDto, BindingResult bindingResult) {
        userValidator.validate(registrationDto, bindingResult);
        if(bindingResult.hasErrors()) {
           Map<String, String> errors = new HashMap();
            bindingResult.getAllErrors().stream().forEach( e -> {
                errors.put(e.getCode(), e.getDefaultMessage());
            });
            Map<String, Map<String,String>> result = new HashMap();
            result.put("errors", errors);
            return new ResponseEntity(result, HttpStatus.BAD_REQUEST);
        }
        User newUser = userService.userRegistration(registrationDto);
        return ResponseEntity.ok(newUser);
    }


    @PostMapping("/auth/login")
    public ResponseEntity<JwtResponse> login(HttpServletRequest req, HttpServletResponse res, @RequestBody JwtRequest authDto) {
        try {
            String username = authDto.getEmail();
            String password = authDto.getPassword();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            User user = userService.findByEmail(username).orElse(null);
            if(user == null) {
                throw new UsernameNotFoundException("Пользователь " + username + " не найден");
            }
            JwtResponse token = authService.login(authDto);
            Cookie cookie = new Cookie("REFRESHTOKEN",token.getRefreshToken());
            cookie.setMaxAge(30 * 24 * 3600 - 30);
            cookie.setHttpOnly(true);
            res.addCookie(cookie);
            //return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, cookie.toString()).body(token);
            return ResponseEntity.ok(token);
        }catch (AuthenticationException | AuthException e) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Неверное имя пользователя или пароль");
            return new ResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
    }

    @GetMapping("/auth/logout")
    public ResponseEntity logout(HttpServletRequest req, HttpServletResponse resp){
        Cookie[] cookies = req.getCookies();
        Cookie refreshCookie = Arrays.stream(cookies).filter(cook -> cook.getName().equals("REFRESHTOKEN")).findFirst().orElse(null);
        if(refreshCookie != null) {
            refreshCookie.setValue(null);
            refreshCookie.setMaxAge(0);
            refreshCookie.setHttpOnly(true);
            resp.addCookie(refreshCookie);
        }
        return ResponseEntity.ok("");
    }


    @GetMapping("auth/refresh")
    public ResponseEntity<JwtResponse> getNewRefreshToken(HttpServletRequest req, HttpServletResponse resp) throws AuthException{
        JwtResponse token = null;
        Cookie refreshCookie = null;
        Cookie[] cookies = req.getCookies();
        try {
            refreshCookie = Arrays.stream(cookies).filter(cook -> cook.getName().equals("REFRESHTOKEN")).findFirst().orElse(null);
            token = authService.refresh(refreshCookie.getValue());
        } catch(Exception e) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Пользователь не авторизован");
            return new ResponseEntity(error, HttpStatus.UNAUTHORIZED);
        }
        Cookie cookie = new Cookie("REFRESHTOKEN",token.getRefreshToken());
        cookie.setMaxAge(30 * 24 * 3600 - 30);
        cookie.setHttpOnly(true);
        resp.addCookie(cookie);
        return ResponseEntity.ok(token);
    }


    @GetMapping("/test")
    public String test(@AuthenticationPrincipal JwtUser jwtUser) {
        User user = userService.findByEmail(jwtUser.getEmail()).orElse(new User());
        return "Hello, " + user.getName() + "! Your roles: " + user.getRoles().toString();
    }

    @GetMapping("/admin/test")
    public String adminTest(@AuthenticationPrincipal JwtUser jwtUser) {
        User user = userService.findByEmail(jwtUser.getEmail()).orElse(new User());
        return "Hello from Admin area, " + user.getName() + "! Your roles: " + user.getRoles().toString();
    }

}
