package top.infoservice.boot3sec.model.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import top.infoservice.boot3sec.dto.RegistrationDto;
import top.infoservice.boot3sec.model.User;
import top.infoservice.boot3sec.service.interfaces.UserService;


@Component
public class UserValidator implements Validator {

    private final UserService userService;

    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return RegistrationDto.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        RegistrationDto newUser = (RegistrationDto) target;
        if(userService.findByEmail(newUser.getEmail()).isPresent())
            errors.rejectValue("email", "email", "Пользователь с таким email адресом уже зарегистрирован");

    }
}
