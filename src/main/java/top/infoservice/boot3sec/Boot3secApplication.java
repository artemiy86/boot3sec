package top.infoservice.boot3sec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Boot3secApplication {

    public static void main(String[] args) {
        SpringApplication.run(Boot3secApplication.class, args);
    }

}
