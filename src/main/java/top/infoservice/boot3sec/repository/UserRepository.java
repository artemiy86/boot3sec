package top.infoservice.boot3sec.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.infoservice.boot3sec.model.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
