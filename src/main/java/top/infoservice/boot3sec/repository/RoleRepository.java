package top.infoservice.boot3sec.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import top.infoservice.boot3sec.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
